@echo off
echo [INFO] Checking for new dependency updates test.skip=true

cd %~dp0
cd ..
call mvn versions:display-dependency-updates -Dmaven.test.skip=true
cd bin
pause