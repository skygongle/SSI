@echo off
echo [INFO] Install jar to local repository. maven.test.skip=true

cd %~dp0
cd ..
call mvn clean source:jar install -Dmaven.test.skip=true
cd bin
pause