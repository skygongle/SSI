package cn.sh.muheng.ssi.web;

import cn.sh.muheng.ssi.service.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @Autowired
    private UserManager userManager;

    private static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        logger.debug("coming  in  login :: GET >>>");
//        userManager.get("1");
        logger.debug("page info : {}", userManager.getAll());
        return "login";
    }
}
