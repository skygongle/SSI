package cn.sh.muheng.ssi.service;

import cn.sh.muheng.ssi.dao.UserMybatisDao;
import cn.sh.muheng.ssi.pojo.UserDTO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserManager {
    @Autowired
    private UserMybatisDao userMybatisDao;

    public UserDTO get(String id) {
        return userMybatisDao.findOne(id);
    }

    public PageInfo getAll() {
        PageHelper.startPage(1,10);
        List<UserDTO> list = userMybatisDao.findAll();
        PageInfo page = new PageInfo(list);
        return page;
    }
}
