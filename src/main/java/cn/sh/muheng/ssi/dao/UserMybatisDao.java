package cn.sh.muheng.ssi.dao;


import cn.sh.muheng.ssi.mybatis.MyBatisRepository;
import cn.sh.muheng.ssi.pojo.UserDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@MyBatisRepository
public interface UserMybatisDao {
    UserDTO findOne(@Param("userId") String userId);

    List<UserDTO> findAll();
}
