package cn.sh;


import cn.sh.muheng.ssi.service.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

//@ContextConfiguration(locations = {"/applicationContext-test.xml"})
@ContextConfiguration(locations = {"/applicationContext.xml"})
@ActiveProfiles("test")
public class ConnTest  extends AbstractJUnit4SpringContextTests {

    @Autowired
    private UserManager userManager;

    @Before
    public void prepareData() {

    }

    @Test
    public void conn() {
        userManager.getAll();
    }

    @Test
    public void create() {

    }

    @Test
    public void update() {

    }

    @Test
    public void delete() {

    }

}
